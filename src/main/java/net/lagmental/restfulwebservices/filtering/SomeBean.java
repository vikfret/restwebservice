package net.lagmental.restfulwebservices.filtering;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@JsonFilter("SomeBeanFilter")
public class SomeBean {
    @Getter @Setter
    private String field1;
    @Getter @Setter
    private String field2;
    @Getter @Setter
    private String field3;
}
