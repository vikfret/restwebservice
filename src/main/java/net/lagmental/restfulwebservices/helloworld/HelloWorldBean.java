package net.lagmental.restfulwebservices.helloworld;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class HelloWorldBean {
    @Getter
    private String message;
}
