package net.lagmental.restfulwebservices.user;

import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class UserDaoService {
    private static List<User> userList = new ArrayList<>();

    private static int usersCount = 3;

    static {
        userList.add(new User(1, "Vicfred", ZonedDateTime.now(ZoneId.of("America/Mexico_City"))));
        userList.add(new User(2, "Eddy", ZonedDateTime.now(ZoneId.of("America/Mexico_City"))));
        userList.add(new User(3, "Chapis", ZonedDateTime.now(ZoneId.of("America/Mexico_City"))));
    }

    public List<User> findAll() {
        return userList;
    }

    public User save(User user) {
        if(null == user.getId()) {
            user.setId(++usersCount);
        }
        userList.add(user);
        return user;
    }

    public User findOne(int id) {
        for(User user: userList) {
            if(user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public User deleteById(int id) {
        Iterator<User> userIterator = userList.iterator();
        while(userIterator.hasNext()) {
            User user = userIterator.next();
            if(user.getId() == id) {
                userIterator.remove();
                return user;
            }
        }
        return null;
    }
}
