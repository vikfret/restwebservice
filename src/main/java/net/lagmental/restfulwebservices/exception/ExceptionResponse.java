package net.lagmental.restfulwebservices.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.ZonedDateTime;

@AllArgsConstructor
public class ExceptionResponse {
    @Setter @Getter
    private ZonedDateTime timestamp;
    @Setter @Getter
    private String message;
    @Setter @Getter
    private String details;
}
